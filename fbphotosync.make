core = 7.x
api = 2

projects[libraries][subdir] = 'contrib'
projects[libraries][version] = '2.0'

projects[token][subdir] = 'contrib'
projects[token][version] = '1.1'

libraries[facebook-php-sdk][download][type] = 'get'
libraries[facebook-php-sdk][download][url] = 'https://github.com/facebook/facebook-php-sdk/zipball/v3.1.1'
